import requests
from django.core.exceptions import PermissionDenied


class SboClient:
    def __init__(
        self,
        hostname: str,
        port: int = 50000,
        database: str = "SBODEMOUS",
        username: str = "manager",
        password: str = "secret",
    ):
        self.service_url = "https://{}:{}/b1s/v1".format(hostname, port)

        # Requests Session
        self.session = requests.Session()
        self.session.verify = False

        # Get session cookie
        login = self.session.post(
            self.service_url + "/Login",
            json={
                "CompanyDB": database,
                "UserName": username,
                "Password": password,
            },
        )

        if login.status_code != 200:
            raise PermissionDenied

    def item_add(self, itemcode, itemname, mdat_id: int, price):
        item = self.session.post(
            self.service_url + "/Items",
            json={
                "ItemCode": itemcode,
                "ItemName": itemname,
                "ItemType": "itItems",
                "U_CO_MDAT_ID": mdat_id,
                "ItemPrices": [
                    {
                        "PriceList": 1,
                        "Price": float(price),
                        "Currency": "EUR",
                        "BasePriceList": 1,
                        "Factor": 1.0,
                    }
                ],
            },
        )

        if item.status_code != 200:
            return False

        return item.json()
